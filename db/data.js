const integrantes = [ //crearmos array JS
    {nombre:"Christian", apellido:"Salinas", codigo:"01", matricula:"Y25366"},
    {nombre:"Lujan",apellido:"Caceres", codigo:"02", matricula:"Y18624"},
    {nombre:"Steven",apellido:"Lopez", codigo:"03", matricula:"Y12887"},
    {nombre:"Marcio",apellido:"Saldivar", codigo:"04", matricula:"3850665"},//objetos JS
]

const tipomedia =[
    {nombre:'YouTube' },
    {nombre:'Imagen'},
    {nombre:'Dibujo'},
];

//Array de Media
const media = [
    {
        //Christian
        titulo: "Página de Christian Salinas", 
        src: "/images/imgrepresentativa.jpg",titulo_imagen:"Imagen que me identifica",
        src_propia: "/images/imgpropia.png",titulo_dibujo:"Imagen que me identifica",
        video: 	"https://www.youtube.com/embed/ZDs_f_ZdluU",
        matricula: "Y25366",
    },
    {
        //Lujan
        titulo: "Página de Lujan Caceres", 
        src: "/images/isla.jpeg",titulo_imagen:"Imagen que me identifica",
        src_propia: "/images/paint_lujan.png",titulo_dibujo:"Imagen que me identifica",
        video: "https://www.youtube.com/embed/Ci5raxp37QE",
        matricula: "Y18624", 
    },
    {
        //Steven
        titulo: "Página de Steven Lopez", 
        src: "/images/mejores-anime-largos.webp",titulo_imagen:"Imagen que me identifica",
        src_propia: "/images/20240410_192335.jpg",titulo_dibujo:"Imagen que me identifica",
        video: "https://www.youtube.com/embed/0aQPX_Iqu-A?si=yrpnMk0IUVFkQtif",
        matricula: "Y12887", 
    },
    {
        //Marcio
        titulo: "Página de Marcio Saldivar", 
        src: "/images/mejores-anime-largos.webp",titulo_imagen:"Imagen que me identifica",
        src_propia: "/images/isla.jpeg",titulo_dibujo:"Imagen que me identifica",
        video: "https://www.youtube.com/embed/QcaZLO59Vpc?si=FUWALVzs0E_DUBKW",
        matricula: "3850665", 
    }, 
];

const home = [
    {
        nombre: "Pagina Princ",
        titulo: "BIENVENIDOS", 
        logo_url: "/images/logo.png",
    },
];

exports.integrantes = integrantes;
exports.tipomedia = tipomedia;
exports.media = media;
exports.home = home;

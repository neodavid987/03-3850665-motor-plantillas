const express = require('express');
const { request } = require("http");
const hbs=require('hbs');
const db=require("./db/data");


//creamos la aplicacion express
const app = express();
const path = require('path');
const internal = require('stream');

app.use(express.static('public'));


app.set( 'view engine' , 'hbs' );
app.set('views', __dirname+'/views');

hbs.registerPartials(__dirname+"/views/partial");
//Raiz
app.get("/", (request,response)=>{
    response.render("index",{integrantes:db.integrantes});
    
});

//Marcio
app.get("/paginas/3850665/index.html", (request,response)=>{
    const matric = "3850665";
    const media = [];
    const personal_info = [];

    //Recorre el array media
    for(i in db.media) {
        if(db.media[i].matricula === matric)
           media.push(db.media[i])
    }
    //Recorre el array integrantes
    for(i in db.integrantes) {
        if(db.integrantes[i].matricula === matric)
        personal_info.push(db.integrantes[i])
    }


    response.render("3850665/index", {
        integrantes:db.integrantes,
        personal_info: personal_info,
        media: media
    });
});

//Lujan
app.get("/paginas/Y18624/index.html", (request,response)=>{
    const matric = "Y18624";
    const media = [];
    const personal_info = [];

    //Recorre el array media
    for(i in db.media) {
        if(db.media[i].matricula === matric)
           media.push(db.media[i])
    }
    //Recorre el array integrantes
    for(i in db.integrantes) {
        if(db.integrantes[i].matricula === matric)
        personal_info.push(db.integrantes[i])
    }


    response.render("Y18624/index", {
        integrantes:db.integrantes,
        personal_info: personal_info,
        media: media
    });
});

//Cristhian
app.get("/paginas/Y25366/index.html", (request,response)=>{
    const matric = "Y25366";
    const media = [];
    const personal_info = [];

    //Recorre el array media
    for(i in db.media) {
        if(db.media[i].matricula === matric)
           media.push(db.media[i])
    }
    //Recorre el array integrantes
    for(i in db.integrantes) {
        if(db.integrantes[i].matricula === matric)
        personal_info.push(db.integrantes[i])
    }


    response.render("Y25366/index", {
        integrantes:db.integrantes,
        personal_info: personal_info,
        media: media
    });
});

//Steven
app.get("/paginas/Y12887/index.html", (request,response)=>{
    const matric = "Y12887";
    const media = [];
    const personal_info = [];

    //Recorre el array media
    for(i in db.media) {
        if(db.media[i].matricula === matric)
           media.push(db.media[i])
    }
    //Recorre el array integrantes
    for(i in db.integrantes) {
        if(db.integrantes[i].matricula === matric)
        personal_info.push(db.integrantes[i])
    }


    response.render("Y12887/index", {
        integrantes:db.integrantes,
        personal_info: personal_info,
        media: media
    });
});

//Wordcloud
app.get("/paginas/wordcloud/index.html", (request,response)=>{
    response.render("wordcloud/index");
});

//Info Curso
app.get("/paginas/info_curso/index.html", (request,response)=>{
    response.render("info_curso/index");
});



// comentaremos esto como prueba
//app.use(express.static(path.join(__dirname, 'public')));

// iniciar app escuchando puerto parametro
app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000");
});